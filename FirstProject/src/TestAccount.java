public class TestAccount {
    public static void main(String[] args) {
//        Account myAccount = new Account();
//        myAccount.setName("James");
//        myAccount.setBalance(100.0);
//
//        System.out.println("Account Name: " + myAccount.getName() + "\n" + "Balance: " + myAccount.getBalance());
//        myAccount.addInterest();
//        System.out.println("Account Name: " + myAccount.getName() + "\n" + "Balance: " + myAccount.getBalance());

        Account[] arrayOfAccounts;

        arrayOfAccounts = new Account[5];

        double[] amounts = {23,5444,2,345,34};
        String[] names = {"Picard", "Ryker", "Worf", "Troy", "Data"};

        for(int i=0; i<arrayOfAccounts.length; i++) {
            arrayOfAccounts[i] = new Account(names[i], amounts[i]);
            arrayOfAccounts[i].addInterest();
            System.out.println("Name: " + arrayOfAccounts[i].getName() + ", Balance: " + arrayOfAccounts[i].getBalance());
        }
    }
}
