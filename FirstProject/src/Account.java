import java.util.ArrayList;

public class Account {
    private double balance;
    private String name;

    public Account(String name, double amount) {
        this.name = name;
        this.balance = amount;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addInterest() {
        balance *= 1.1;
    }
}
