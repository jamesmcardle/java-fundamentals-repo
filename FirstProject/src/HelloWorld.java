package com.conygre.simple;

import java.lang.*;
import java.util.ArrayList;
import java.util.List;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello from my first Java program!");
        List<Integer> myArray = new ArrayList<Integer>();
        String make = "Volkswagen";
        String model = "Golf";
        Double engineSize = 1.6;
        byte gear = 2;

        System.out.println("The make is " + make + "\n" + "The model is " + model + "\n" + "The engine size is " + engineSize);

        short speed = (short)(gear * 20.0);

        System.out.println("The speed is " + speed);

        if (engineSize < 1.3) {
            System.out.println("Car is weak.");
        } else {
            System.out.println("Car is powerful.");
        }

        if (gear == 1) {
            System.out.println("Gear is " + gear + ", max speed should be 10mph.");
        } else if (gear == 2) {
            System.out.println("Gear is " + gear + ", max speed should be 15mph.");
        } else if (gear == 3) {
            System.out.println("Gear is " + gear + ", max speed should be 25mph.");
        } else if (gear == 4) {
            System.out.println("Gear is " + gear + ", max speed should be 30mph.");
        } else if (gear == 5) {
            System.out.println("Gear is " + gear + ", max speed should be 45mph.");
        } else {
            System.out.println("Gears are broken.");
        }

        int count = 0;
        for(int year = 1900; year<=2000; year++){
            if(year % 4 == 0){
                count++;
                System.out.println(year);
                myArray.add(year);
                if(count == 10){
                    System.out.println("Finished.");
                    break;
                }
            }
        }

        switch (gear){
            case 1:
                System.out.println("Gear is " + gear + ", max speed should be 10mph.");
                break;
            case 2:
                System.out.println("Gear is " + gear + ", max speed should be 15mph.");
                break;
            case 3:
                System.out.println("Gear is " + gear + ", max speed should be 25mph.");
                break;
            case 4:
                System.out.println("Gear is " + gear + ", max speed should be 35mph.");
                break;
            case 5:
                System.out.println("Gear is " + gear + ", max speed should be 45mph.");
                break;
            default:
                System.out.println("Gears are broken.");
                break;
        }

        for(int i : myArray){
            System.out.println(i);
        }

    }
}
